# wunder checkout test

### Requirements

* Docker

### Installation

```shell
docker-compose up -d --build

# run the migrations db
docker exec -it wunder-checkout bin/console doctrine:migrations:migrate
```

Access via: http://localhost:8010

### How was done

* PHP 8.0.3
* SQLite 3  
* Symfony 5

#### Explanations

I've tried to use a perspective of hexagonal architecture, mixed with some aspects of DDD.

My Aggregate root always is the Customer (main user). Through it, you can add the information in each step.

You can also test the application using PHPUnit.

Command:

```shell
docker exec -it wunder-checkout vendor/bin/phpunit
```

*What I would have done better?*

Within more time, tried more other approaches, like CQRS and Message aspects.
