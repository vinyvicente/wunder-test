<?php
declare(strict_types=1);

namespace App\Domain;

class PaymentDataId
{
    public function __construct(private string $paymentDataId)
    {
    }

    public function getPaymentDataId(): string
    {
        return $this->paymentDataId;
    }
}
