<?php
declare(strict_types=1);

namespace App\Application\Write;

use App\Domain\Command;
use Symfony\Component\Validator\Constraints as Assert;

class SaveAddressInfoCommand implements Command
{
    /**
     * @Assert\NotBlank
     */
    public string $customerId;

    /**
     * @Assert\NotBlank
     */
    public string $street;

    /**
     * @Assert\NotBlank
     */
    public string $houseNumber;

    /**
     * @Assert\NotBlank
     */
    public string $zipCode;

    /**
     * @Assert\NotBlank
     */
    public string $city;
}
