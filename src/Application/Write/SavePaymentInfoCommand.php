<?php
declare(strict_types=1);

namespace App\Application\Write;

use App\Domain\Command;
use Symfony\Component\Validator\Constraints as Assert;

class SavePaymentInfoCommand implements Command
{
    /**
     * @Assert\NotBlank
     */
    public string $customerId;

    /**
     * @Assert\NotBlank
     */
    public string $accountOwner;

    /**
     * @Assert\NotBlank
     */
    public string $iban;
}
