<?php
declare(strict_types=1);

namespace App\Infra\Repository;

use App\Domain\Payment;
use App\Domain\PaymentDataId;
use App\Domain\PaymentGatewayRepository;
use Exception;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class PaymentGatewayRepositoryThirdPart implements PaymentGatewayRepository
{
    public function __construct(
        private HttpClientInterface $httpClient,
        private SerializerInterface $serializer
    )
    {
    }

    public function processPayment(Payment $payment, string $customerId): void
    {
        try {
            $response = $this->httpClient
                ->request('POST',
                    'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data',
                    [
                        'json' => [
                            'customerId' => $customerId,
                            'iban' => $payment->getIban(),
                            'owner' => $payment->getOwner(),
                        ]
                    ]
                );

            /** @var PaymentDataId $paymentData */
            $paymentData = $this->serializer->deserialize(
                $response->getContent(),
                'App\Domain\PaymentDataId',
                'json'
            );

            $payment->setPaymentId($paymentData->getPaymentDataId());

        } catch (Exception | TransportExceptionInterface $e) {
        }
    }
}
