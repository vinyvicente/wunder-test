<?php
declare(strict_types=1);

namespace App\Infra\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RefererURISubscriber implements EventSubscriberInterface
{
    public function __construct(private SessionInterface $session)
    {
    }

    public function onRequest(RequestEvent $event): void
    {
        if ($this->isAllowedToRedirect($event)) {
            $event->setResponse(new RedirectResponse($this->session->get('lastStep')));
        }
    }

    public function isAllowedToRedirect(RequestEvent $event): bool
    {
        if (false === $this->session->has('lastStep')) {
            return false;
        }

        if (false === $event->getRequest()->isMethod('GET')) {
            return false;
        }

        if ($event->getRequest()->getPathInfo() === $this->session->get('lastStep')) {
            return false;
        }

        return true;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onRequest',
        ];
    }
}
