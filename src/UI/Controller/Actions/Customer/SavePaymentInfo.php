<?php
declare(strict_types=1);

namespace App\UI\Controller\Actions\Customer;

use App\Application\Write\SaveAddressInfoCommand;
use App\Application\Write\SavePaymentInfoCommand;
use App\Domain\CustomerRepository;
use App\Domain\Payment;
use App\Domain\PaymentGatewayRepository;
use App\UI\Controller\RefererUri;
use App\UI\Form\Type\PaymentInfoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(path="/{id}/payment", name="save_payment_info", methods={"POST"})
 */
class SavePaymentInfo extends AbstractController
{
    public function __construct(
        private CustomerRepository $repository,
        private PaymentGatewayRepository $gatewayRepository,
        private SessionInterface $session
    )
    {
    }

    public function __invoke(Request $request, string $id): RedirectResponse
    {
        $customer = $this->repository->retrieve($id);

        $form = $this->createForm(PaymentInfoType::class, new SavePaymentInfoCommand());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var SavePaymentInfoCommand $event */
            $event = $form->getData();
            $payment = new Payment($event->iban, $event->accountOwner);

            $customer->setPaymentInfo($payment);

            $this->gatewayRepository->processPayment($payment, $customer->getId());
            $this->repository->save($customer);

            $this->session->remove('lastStep');

            return $this->redirectToRoute('final_step', ['id' => $customer->getId()]);
        }

        return $this->redirectToRoute('personal_info');
    }
}
