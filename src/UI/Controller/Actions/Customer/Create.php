<?php
declare(strict_types=1);

namespace App\UI\Controller\Actions\Customer;

use App\Application\Write\SavePersonalInfoCommand;
use App\Domain\Customer;
use App\Domain\CustomerRepository;
use App\UI\Controller\RefererUri;
use App\UI\Form\Type\PersonalInfoType;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="save_personal_info", methods={"POST"})
 */
class Create extends AbstractController
{
    use RefererUri;

    public function __construct(private CustomerRepository $repository, private SessionInterface $session)
    {
    }

    public function __invoke(Request $request): RedirectResponse
    {
        $form = $this->createForm(PersonalInfoType::class, new SavePersonalInfoCommand(Uuid::uuid4()->toString()));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var SavePersonalInfoCommand $event */
            $event = $form->getData();

            $customer = new Customer($event->uuid, $event->firstName, $event->lastName, $event->telephone);

            $this->repository->save($customer);

            return $this->redirectToRoute('address_info', ['id' => $customer->getId()]);
        }

        return $this->redirectToRoute('personal_info');
    }
}
