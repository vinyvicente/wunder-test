<?php
declare(strict_types=1);

namespace App\UI\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

trait RefererUri
{
    protected function redirectToRoute(string $route, array $parameters = [], int $status = 302): RedirectResponse
    {
        if ($this->session instanceof SessionInterface) {
            $this->session->remove('lastStep');
            $this->session->set('lastStep', $this->generateUrl($route, $parameters));
        }

        return $this->redirect($this->generateUrl($route, $parameters), $status);
    }
}
