<?php
declare(strict_types=1);

namespace App\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegisterFormTest extends WebTestCase
{
    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = self::createClient();
    }

    /** @test */
    public function can_show_form_creation()
    {
        $this->client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorExists('form[name="personal_info"]');
    }

    /** @test */
    public function can_create_customer_all_steps()
    {
        $crawler = $this->client->request('GET', '/');

        $form               = $crawler->selectButton('Next')->form();
        $form['personal_info[firstName]']  = 'Max';
        $form['personal_info[lastName]']   = 'Müller';
        $form['personal_info[telephone]']  = '+491522433888';

        $crawler = $this->client->submit($form);

        preg_match('/\/(.+)\//', $crawler->html(), $matches);

        $uuid = $matches[1];

        $this->assertResponseRedirects('/' . $uuid . '/address');

        $crawler = $this->client->request('GET', '/' . $uuid . '/address');

        $form                                = $crawler->selectButton('Next')->form();
        $form['address_info[street]']        = 'Borgfeld str.';
        $form['address_info[houseNumber]']   = '11A';
        $form['address_info[zipCode]']       = '20537';
        $form['address_info[city]']          = 'Hamburg';

        $this->client->submit($form);

        $this->assertResponseRedirects('/' . $uuid . '/payment');

        $crawler = $this->client->request('GET', '/' . $uuid . '/payment');

        $form                                = $crawler->selectButton('Finish')->form();
        $form['payment_info[accountOwner]']  = 'Max Mueller';
        $form['payment_info[iban]']          = 'DE5489584985498';

        $this->client->submit($form);

        $this->assertResponseRedirects('/' . $uuid . '/details');
    }

    /** @test */
    public function can_back_to_last_step()
    {
        $crawler = $this->client->request('GET', '/');

        $form               = $crawler->selectButton('Next')->form();
        $form['personal_info[firstName]']  = 'Max';
        $form['personal_info[lastName]']   = 'Müller';
        $form['personal_info[telephone]']  = '+491522433888';

        $crawler = $this->client->submit($form);

        preg_match('/\/(.+)\//', $crawler->html(), $matches);

        $uuid = $matches[1];

        $this->assertResponseRedirects('/' . $uuid . '/address');

        $crawler = $this->client->request('GET', '/' . $uuid . '/address');

        $form                                = $crawler->selectButton('Next')->form();
        $form['address_info[street]']        = 'Borgfeld str.';
        $form['address_info[houseNumber]']   = '11A';
        $form['address_info[zipCode]']       = '20537';
        $form['address_info[city]']          = 'Hamburg';

        $this->client->submit($form);
        $this->client->request('GET', '/');

        $this->assertResponseRedirects('/' . $uuid . '/payment');
    }
}
