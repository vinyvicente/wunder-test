<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain;

use App\Domain\Address;
use App\Domain\Customer;
use App\Domain\Payment;
use PHPUnit\Framework\TestCase;

class CustomerTest extends TestCase
{
    /** @test */
    public function can_create_customer_all_domain_entities(): void
    {
        $customer = new Customer('123', 'Max', 'Mueller', '152289898');

        $this->assertEquals('123', $customer->getId());
        $this->assertEquals('Max', $customer->getFirstName());
        $this->assertEquals('Mueller', $customer->getLastName());
        $this->assertEquals('152289898', $customer->getTelephone());

        $address = new Address('Hamburg str.', '14', '20122', 'Hamburg');

        $customer->setAddressInfo($address);

        $this->assertInstanceOf(Address::class, $customer->getAddress());

        $payment = new Payment('DE43894943989843', 'Max Mueller');

        $customer->setPaymentInfo($payment);

        $this->assertInstanceOf(Payment::class, $customer->getPayment());

        $this->assertEquals($payment, $customer->getPayment());
        $this->assertEquals($address, $customer->getAddress());
    }
}
