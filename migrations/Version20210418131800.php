<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210418131800 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE address (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, customer_id VARCHAR(40) DEFAULT NULL, street VARCHAR(100) NOT NULL, house_number VARCHAR(10) NOT NULL, zip_code VARCHAR(5) NOT NULL, city VARCHAR(100) NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4E6F819395C3F3 ON address (customer_id)');
        $this->addSql('CREATE TABLE customer (id VARCHAR(40) NOT NULL, first_name VARCHAR(100) NOT NULL, last_name VARCHAR(155) NOT NULL, telephone VARCHAR(20) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE payment (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, customer_id VARCHAR(40) DEFAULT NULL, payment_data_id VARCHAR(100) NOT NULL, iban VARCHAR(100) NOT NULL, owner VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6D28840D9395C3F3 ON payment (customer_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP TABLE payment');
    }
}
